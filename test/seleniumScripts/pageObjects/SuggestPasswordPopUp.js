import PopUp from './baseClasses/PopUp'

class SuggestPasswordPopUp extends PopUp {
  get popUpDiaog() {
    return super.popUpDialog
  }

  get okBtn() {
    return this.popUpDiaog.$('div .ui-dialog-buttonset').$('.primary')
  }

  get closeBtn() {
    return super.closeBtn()
  }

  get closeDialogBtn() {
    return super.closeDialogBtn()
  }

  get suggestedPassword() {
    return this.popUpDiaog.$('div #insly_alert').$('b')
  }
}

export default new SuggestPasswordPopUp()
