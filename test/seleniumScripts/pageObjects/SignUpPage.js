import Page from './baseClasses/Page'

class SignUpPage extends Page {
  open() {
    super.open()
  }

  login() {
    super.login()
  }

  get title() {
    return browser.getTitle()
  }

  // Reason for using body + id is that id selectors are about to be deprecated for webdriver/selenium.
  // The trick of inserting body makes webdriverIO to use css selectors method instead
  get companyName() {
    return $('body #broker_name')
  }

  get countrySelector() {
    return $('body #broker_address_country')
  }

  selectCountry(i) {
    return this.countrySelector.$$('option')[i]
  }

  get inslyAddress() {
    return $('body #broker_tag')
  }

  get companyProfileSelector() {
    return $('body #prop_company_profile')
  }

  selectCompanyProfile(i) {
    return this.companyProfileSelector.$$('option')[i]
  }

  get numberOfEmployeesSelector() {
    return $('body #prop_company_no_employees')
  }

  selectNumberEmployees(i) {
    return this.numberOfEmployeesSelector.$$('option')[i]
  }

  get describeYourselfSelector() {
    return $('body #prop_company_person_description')
  }

  get suggestSecurePasswordLink() {
    return $('body #field_broker_person_password').$('a')
  }

  selectDescription(i) {
    return this.describeYourselfSelector.$$('option')[i]
  }

  get addNewBrokerNote() {
    return $('body #field_add_newbroker_note')
  }

  get iconOK() {
    return $('span .icon-ok')
  }

  get adminWorkEmail() {
    return $('body #broker_admin_email')
  }

  get accoutManagerName() {
    return $('body #broker_admin_name')
  }

  get password() {
    return $('body #broker_person_password')
  }

  get repeatPassword() {
    return $('body #broker_person_password_repeat')
  }

  get phone() {
    return $('body #broker_admin_phone')
  }

  get fieldTerms() {
    return $('body #field_terms')
  }

  get termsCheckbox() {
    return this.fieldTerms.$$('.icon-check-empty')[0]
  }

  get privacyCheckbox() {
    return this.fieldTerms.$$('.icon-check-empty')[1]
  }

  get dataCheckbox() {
    return this.fieldTerms.$$('.icon-check-empty')[2]
  }

  get termsLink() {
    return this.fieldTerms.$$('a')[0]
  }

  get privacyLink() {
    return this.fieldTerms.$$('a')[1]
  }

  get submitBtn() {
    return $('body #submit_save')
  }
}

export default new SignUpPage()
