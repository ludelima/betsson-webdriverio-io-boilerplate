import PopUp from './baseClasses/PopUp'

class TermsPopUp extends PopUp {
  get termsPopUpDialog() {
    return $$('ui-dialog')[1]
  }

  get agreeBtn() {
    return $('.ui-dialog-buttonset').$('.primary')
  }

  get closeBtn() {
    super.closeBtn
  }

  get closeDialogBtn() {
    super.closeDialogBtn
  }
}

export default new TermsPopUp()
