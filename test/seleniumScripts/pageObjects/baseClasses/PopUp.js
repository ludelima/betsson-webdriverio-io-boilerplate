export default class PopUp {
  get closeBtn() {
    return $('span .icon-close')
  }

  get closeDialogBtn() {
    return $('body ui-dialog-buttonset').$$('button')[1]
  }

  get popUpDialog() {
    return $$('.ui-dialog')[0]
  }
}
