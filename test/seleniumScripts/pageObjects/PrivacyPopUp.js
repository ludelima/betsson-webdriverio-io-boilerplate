import PopUp from './baseClasses/PopUp'

class PrivacyPopUp extends PopUp {
  get privacyPopUpDialog() {
    return $$('.ui-dialog')[1]
  }

  get privacyText() {
    return this.privacyPopUpDialog.$('div #document-content')
  }

  get closeBtn() {
    return this.privacyPopUpDialog.$('.icon-close')
  }
}
export default new PrivacyPopUp()
