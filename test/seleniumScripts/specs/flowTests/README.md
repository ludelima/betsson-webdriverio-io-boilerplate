

# Insly E2E tests

## Table of Contents

- [Dependencies](#dependencies)
- [Prerequisites](#prerequisites)
- [Available Scripts](#available-scripts)

### Dependencies

This project uses a number of open source projects to work properly:

- [WebdriverIO](https://webdriver.io/) - A Node.js E2E testing framework built on top of selenium
- [Mocha](https://mochajs.org) - Javascript testing framework
- [Chai](https://chaijs.com) - Assertion lib for Node.js

The project can run on [Browserstack cloud service](https://www.browserstack.com/). Just modify file browserstack.conf.js with the credentials and you'll be good to go!

### Prerequisites

We use [Yarn](https://yarnpkg.com/en/) for package management.

Before start to coding and run, chose and execute the preparation script for environment,

The command is: `yarn env-{target}`

For {target}, we currently have the following options: `prod` (ex: `yarn env-prod`)

This will create the file `<rootDir>/env.json` with environment variables needed for a given environment.

You can customize all the generated value as you want because this generated file is not versioned.
More information [in the official repository](https://github.com/benhurott/app-json-env-gen)


### Available scripts

- `yarn test` - Run all selenium tests locally
- `yarn test-browserstack` - Run both selenium and appium tests on browserstack-cloud
- `yarn format` - runs *prettier* all over the project

---

Lucas